<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 21.16
 */
?>
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Users Identification</title>
  <meta name="description" content="Users identification app">
  <meta name="author" content="Bojan Lazarevic">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="public/css/styles.css?v=1.0">

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body>

    <section class="main">
        <?php run($controller, $action, $_POST); ?>
    </section>

    <script src="public/js/scripts.js"></script>
</body>
</html>