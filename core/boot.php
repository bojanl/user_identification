<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 19.16
 */
session_start();

spl_autoload_register(function ($class) {
    include($class . ".php");
});