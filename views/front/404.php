<div class="not-found">
</div>

<div class="not-found">
    <h1>User identification app</h1>

    <h2>404</h2>

    <h3 class="message">Page not found</h3>

    <div class="button-holder">
        <a href="?controller=front&action=index" class="button">Home</a>
    </div>
</div>

