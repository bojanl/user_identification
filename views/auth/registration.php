<form class="register" role="form" method="POST" action="?controller=auth&action=post_register">

    <h2>Registration</h2>

    <?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){ ?>
        <h3 class="message"><?=$_SESSION['message']?></h3>
    <?php } ?>


    <div class="form-group">
        <label for="name" class="col-md-4 control-label">Name</label>
        <input id="name" type="text" class="form-control" name="name" value="" autofocus>
    </div>

    <div class="form-group">
        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
        <input id="email" type="email" class="form-control" name="email" value="">
    </div>

    <div class="form-group">
        <label for="password" class="col-md-4 control-label">Password</label>
        <input id="password" type="password" class="form-control" name="password">
    </div>

    <div class="form-group">
        <label for="password-confirm">Confirm Password</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
    </div>

    <div class="form-group">
        <div class="button-holder">
            <button type="submit" class="btn">Register</button>
        </div>

        <div class="button-holder">
            <a href="?controller=front&action=index" class="button">Home</a>
        </div>
    </div>
</form>

