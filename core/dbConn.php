<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 19.02
 */

namespace core;

class dbConn
{
    public $conn = NULL;

    private $db_name;
    private $db_user;
    private $db_pass;
    private $db_host;

    public function __construct(){

        $conf = new config;

        $this->db_name = $conf->getEnvVar('DB_NAME');
        $this->db_user = $conf->getEnvVar('DB_USER');
        $this->db_pass = $conf->getEnvVar('DB_PASS');
        $this->db_host = $conf->getEnvVar('DB_HOST');

        //set options
        $options = array(
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        );

        // create new PDO instance
        try {
            $this->conn = new \PDO ("mysql:host={$this->db_host};dbname={$this->db_name}", $this->db_user, $this->db_pass, $options);
        }
        catch ( \PDOException $e) {
            $this->error = $e->getMessage();
        }
    }


}

