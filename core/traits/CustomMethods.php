<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 20:49
 */

namespace core\traits;

trait CustomMethods {

    public static function clean($string, $soft_clean = false, $allowable_tags = null){

        $string = trim($string);

        if($soft_clean){
            $string = htmlspecialchars($string);
        } else {
            $string = htmlentities($string);
        }

        $string = strip_tags($string, $allowable_tags);

        return $string;
    }


    public static function salt_password($password){

        $new_password = SHA1($password). MD5($password);
        $string = substr($new_password, 28,30);
        return self::clean($string);

    }


    public static function format_date($date){

        $new_date = date("d.m.Y H:i:s", strtotime($date));

        return $new_date;
    }

} 