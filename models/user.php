<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 23.59
 */

namespace models;

use core\dbConn;


class user
{

    public $db;

    public $id;
    public $name;
    public $email;
    public $created_at;

    public function __construct() {
        $this->db = (new dbConn)->conn;
    }


    public function set_user(){

        if (isset($_SESSION['admin_id'])){

            $q = $this->db->prepare("SELECT * FROM users WHERE id = :id");
            $q->execute(array(':id' => intval($_SESSION['admin_id']) ));
            $admin = $q->fetchColumn();

            if ($admin->id) {
                $this->id           = $admin->id;
                $this->name         = $admin->name;
                $this->email        = $admin->email;
                $this->created_at   = $admin->created_at;
            }

            return true;
        }

        return false;
    }

    public function userExists($email) {

        $q = $this->db->prepare("SELECT email FROM users WHERE email = :email");
        $q->execute(array(':email' => $email ));

        if(!empty($q->fetchColumn())){
            return true;
        }

        return false;
    }

    public function checkEmailPass($email, $password) {

        $q = $this->db->prepare("SELECT id FROM users WHERE email = :email AND password = :password");
        $q->execute(array(':email' => $email, ':password' => $password ));
        $id = $q->fetchColumn();

        if(!empty($id)){
            return $id;
        }

        return false;
    }


    public function createUser($user_data) {
        $statement = $this->db->prepare("INSERT INTO users(name, email, password)
    VALUES(:name, :email, :password)");
        $statement->execute(array(
            "name" => $user_data['name'],
            "email" => $user_data['email'],
            "password" => $user_data['password']
        ));
    }


    public function getUserById($id) {
        $q = $this->db->prepare("SELECT * FROM users WHERE id = :id");
        $q->execute(array(':id' => $id ));
        $user = $q->fetch();

        if(!empty($user['id'])){
            return $user;
        }

        return false;
    }


    public function getAllUsers() {
        $q = $this->db->query("SELECT * FROM users ORDER BY created_at DESC");
        $users = $q->fetchAll();

        if(!empty($users)){
            return $users;
        }

        return false;
    }

}