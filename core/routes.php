<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 20.42
 */


require_once('boot.php');

function run($controller, $action, $data = null) {

    switch($controller) {
        case 'auth':
            $controller = new controllers\authController;
            break;
        case 'users':
            // we need the model to query the database later in the controller
//            require_once('models/user.php');
            $controller = new controllers\usersController;
            break;
        case 'front':
            $controller = new controllers\frontController;
            break;
        case 'home':
            $controller = new controllers\homeController;
            break;
    }

    $controller->{ $action }($data);
}



$controllers = array(
    'auth'  => ['login', 'post_login', 'post_logout', 'register', 'post_register'],
    'home'  => ['index'],
    'front' => ['index', 'not_found'],
    'users' => ['index']
);


if (isset($_GET['controller']) && isset($_GET['action'])) {

    if (array_key_exists($_GET['controller'], $controllers)) {
        if (in_array($_GET['action'], $controllers[$_GET['controller']])) {
            $controller = $_GET['controller'];
            $action     = $_GET['action'];
        } else {
            $controller = 'front';
            $action     = 'not_found';
        }
    } else {
        $controller = 'front';
        $action     = 'not_found';
    }

} else {
    $controller = 'front';
    $action     = 'index';
}

//require_once($conf->getEnvVar('PROJECT_PATH') . 'views/layout.php');
require_once('views/layout.php');

$_SESSION['message'] = '';

