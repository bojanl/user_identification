<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 21.32
 */

namespace controllers;


class frontController
{

    public function index(){

        require_once('views/front/welcome.php');
    }

    public function not_found(){

        require_once('views/front/404.php');
    }

}