<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 21.09
 */

namespace controllers;

use models\user;
use core\traits\CustomMethods;


class usersController
{

    use CustomMethods; // trait

    public $admin_id;
    public $user;

    public function __construct(){
        if(!isset($_SESSION['admin_id']) || empty($_SESSION['admin_id'])){

            header("Location: ?controller=front&action=index");
            exit;
        }

        $this->admin_id = $_SESSION['admin_id'];
        $this->user = new user;
    }

    public function index(){
        $users = $this->user->getallUsers();

        require_once('views/admin/users.php');

    }

}