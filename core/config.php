<?php

/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 18.25
 */

namespace core;


class Config
{

    /**
     * @param string $search
     * @param null $default
     * @param string $envFilePath
     * @return mixed (string or null)
     */
    function getEnvVar($search, $default = null, $envFilePath = 'core/.env'){
        $f_handle = fopen($envFilePath, "r");
        $arr_params = [];

        while (!feof($f_handle)) {
            $line = trim(fgets($f_handle));
            if(!empty($line) && strpos($line, '=') !== false) {
                $arr_param_value = explode('=', $line);
                $arr_params[$arr_param_value[0]] = $arr_param_value[1];
            }
        }

        if(array_key_exists($search, $arr_params)){
            return $arr_params[$search];
        }

        return $default;
    }
}