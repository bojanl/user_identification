<?php require_once('_partials/nav.php'); ?>

<div class="users">
    <?php if(!empty($users)){ ?>
        <table id="t01">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
            </tr>
            <?php foreach($users as $user){ ?>
                <tr>
                    <td><?=$user['name']?></td>
                    <td><?=$user['email']?></td>
                    <td><?=self::format_date($user['created_at'])?></td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
</div>

