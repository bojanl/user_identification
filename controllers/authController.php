<?php
/**
 * Created by PhpStorm.
 * User: Bojan
 * Date: 26.9.2017.
 * Time: 20.47
 */

namespace controllers;

use core\config;
use core\traits\CustomMethods;
use models\user;


class authController
{

    use CustomMethods; // trait

    public $id;
    public $name;

    public $conf;
    public $user;


    public function __construct(){
        $this->conf = new \core\config;
        $this->user = new user;
    }


    public function register(){

        require_once('views/auth/registration.php');
    }


    public function post_register($user_data){

        // cleaning data
        $user_data = array_map('self::clean', $user_data);

        // checking whether the fields are filled
        if(empty($user_data['name']) || empty($user_data['email']) || empty($user_data['password']) || empty($user_data['password_confirmation'])){
            $_SESSION['message'] = "All the fields needs to be filled.";
            header("Location: ?controller=auth&action=register");
            exit;
        }

        // check pass matching
        if($user_data['password'] != $user_data['password_confirmation']){
            $_SESSION['message'] = "Passwords don't match.";
            header("Location: ?controller=auth&action=register");
            exit;
        }

        // check email format
        if (!filter_var($user_data['email'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['message'] = "Invalid email format";
            header("Location: ?controller=auth&action=register");
            exit;
        }

        // check if email exists
        if($this->user->userExists($user_data['email'])){
            $_SESSION['message'] = "Email exists.";
            header("Location: ?controller=auth&action=register");
            exit;
        }

        $user_data['password'] = self::salt_password($user_data['password']);

        $this->user->createUser($user_data);

        $this->login($user_data);

        $_SESSION['message'] = "You are registered successfully!";
        header("Location: ?controller=home&action=index");
        exit;

    }


    public function login(){

        require_once('views/auth/login.php');
    }


    public function post_login($user_data){

        // cleaning data
        $user_data = array_map('self::clean', $user_data);

        // checking whether the fields are filled
        if(empty($user_data['email']) || empty($user_data['password'])){
            $_SESSION['message'] = "All the fields needs to be filled.";
            header("Location: ?controller=auth&action=login");
            exit;
        }

        // check email format
        if (!filter_var($user_data['email'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['message'] = "Invalid email format";
            header("Location: ?controller=auth&action=login");
            exit;
        }

        $admin_id = $this->user->checkEmailPass($user_data['email'], self::salt_password($user_data['password']));

        if (isset($admin_id) && !empty($admin_id)){
            $_SESSION['admin_id'] = $admin_id;
            $this->user->set_user();

            $_SESSION['message'] = 'Login successful!';
            header("Location: ?controller=home&action=index");
            exit;

        } else {
            $_SESSION['message'] = 'Username or password are not correct!';
            header("Location: ?controller=auth&action=login");
            exit;

        }
    }


    public function post_logout(){
        $_SESSION = array();
        unset($_SESSION['admin']);

        $_SESSION['message'] = 'You are signed out.';
        header("Location: ?controller=auth&action=login");
        exit;
    }

}