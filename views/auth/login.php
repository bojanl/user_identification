<form class="login" role="form" method="POST" action="?controller=auth&action=post_login">

    <h2>Login</h2>

    <?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){ ?>
        <h3 class="message"><?=$_SESSION['message']?></h3>
    <?php } ?>

    <div class="form-group">
        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
        <input id="email" type="email" class="form-control" name="email" value="">
    </div>

    <div class="form-group">
        <label for="password" class="col-md-4 control-label">Password</label>
        <input id="password" type="password" class="form-control" name="password">
    </div>

    <div class="form-group">
        <div class="button-holder">
            <button type="submit" class="btn">Login</button>
        </div>

        <div class="button-holder">
            <a href="?controller=front&action=index" class="button">Home</a>
        </div>
    </div>
</form>

