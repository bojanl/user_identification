<div class="welcome">
    <h1>User identification app</h1>

    <?php if(isset($_SESSION['message']) && !empty($_SESSION['message'])){ ?>
        <h3 class="message"><?=$_SESSION['message']?></h3>
    <?php } ?>

    <h3>Please choose one of the options:</h3>


    <div class="button-holder">
        <a href="?controller=auth&action=login" class="button">Login</a>
    </div>

    <div class="button-holder">
        <a href="?controller=auth&action=register" class="button">Register</a>
    </div>
</div>

